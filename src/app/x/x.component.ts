import { HttpClient } from '@angular/common/http';
import { Component, ComponentFactoryResolver, OnInit, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-x',
  templateUrl: './x.component.html',
  styleUrls: ['./x.component.css']
})
export class XComponent implements OnInit {

  constructor(private vcRef:ViewContainerRef, private resolver:ComponentFactoryResolver, private hc:HttpClient) { }

  names:any;
  ngOnInit(): void {
    this.hc.get('/username').subscribe((response)=>{
      console.log(response)
      this.names=response["names"]
      console.log(this.names)
    })
  }

  async addCompontnY(){
    this.vcRef.clear();
    const {YComponent}=await import('../y.component');
    this.vcRef.createComponent(this.resolver.resolveComponentFactory(YComponent))
  }
}
