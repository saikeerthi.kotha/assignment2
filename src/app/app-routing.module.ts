import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { XComponent } from './x/x.component';


const routes: Routes = [
  {path:"x",component:XComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
