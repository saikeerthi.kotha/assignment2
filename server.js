const exp=require("express");
const app=exp();

//importing user App
const userApp=require('./apis/userApi');
app.use('/',userApp)

//connect to angular app
const path=require("path");
app.use(exp.static(path.join(__dirname,'./dist/assignment2')))

//assign port no
const port=2000;
app.listen(port,()=>{
    console.log(`server is running on ${port}`)
})